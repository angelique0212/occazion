<?php

if (!function_exists("addStyle")) {
    function addStyle()
    {

        wp_enqueue_style("bootstrap", "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css");
        wp_enqueue_style("font-awesome", "https://use.fontawesome.com/releases/v5.6.3/css/all.css");
        wp_enqueue_script("bootstrap", "https://code.jquery.com/jquery-3.2.1.slim.min.js");
        wp_enqueue_script("bootstrap", "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js");
        wp_enqueue_script("bootstrap", "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js");
        wp_enqueue_style("my-stylesheet", get_stylesheet_directory_uri() . '/style.css');
    }
}
add_action("wp_enqueue_scripts", "addStyle");

register_nav_menus( [
    "nav_menu"=>__('menu', 'enssop'),
    "footer_menu" => __('Menu de pied de page', 'enssop')
] );
function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

?>