<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
   *{ box-sizing:border-box;
   margin:0;
   }
    
    .logo{
        height: 60px;
        margin-top:40px;
        margin:auto 40.5%;
}

.menu-menu-container li{
    list-style-type: none;
    font-size: 30px;
    color:#ff4f00;
  }


.menu-menu-container{
    background-color: black;
    padding:30px;
    margin-top:100px;
   }

   a{
       color:#ff4f00;
       text-decoration: none;
       padding:115px;
       margin:auto;
   }
li{
    display: inline-block;
    
}
li ul{
    position:absolute;
    
}
li li {
    display:none;
}

li:hover li{
    display: block;
    margin-left: -25px;
}

.sub-menu li a{
    color:#ff4f00;
    font-size:20px;

}

span{
    color:white;
}
 #nav>#entete {
     background-color: black;
    border-top-style: none;
    text-align: right;
    top: -155px;
    padding:15px;
}

</style>
<body>
    <div id="nav">
        <div id="entete">
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-people" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.995-.944v-.002.002zM7.022 13h7.956a.274.274 0 0 0 .014-.002l.008-.002c-.002-.264-.167-1.03-.76-1.72C13.688 10.629 12.718 10 11 10c-1.717 0-2.687.63-3.24 1.276-.593.69-.759 1.457-.76 1.72a1.05 1.05 0 0 0 .022.004zm7.973.056v-.002.002zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10c-1.668.02-2.615.64-3.16 1.276C1.163 11.97 1 12.739 1 13h3c0-1.045.323-2.086.92-3zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"/>
</svg>
           
            <li class="login" id="login"><a href="page-moncompte.php" title="connexion"><span>MON COMPTE, SE CONNECTER</span></a>
        
            </li>
            <li class="login" id="login"><a href="page-inscription.php" title="inscription"><span>s'inscrire</span></a>
        
        </li>
        </ul>
        </div>
    </div> 

<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/IMAGES/Logosite.png" alt="">
<?php wp_nav_menu( [
               "theme_location" => "nav_menu"
               ])?>